# Edit this configuration file to define what should be installed on
# your system. Help is available in the configuration.nix(5) man page, on
# https://search.nixos.org/options and in the NixOS manual (`nixos-help`).

# NixOS-WSL specific options are documented on the NixOS-WSL repository:
# https://github.com/nix-community/NixOS-WSL

{ config, lib, pkgs, ... }:

{
  imports = [
    # include NixOS-WSL modules
    <nixos-wsl/modules>
  ];

  wsl.enable = true;
  wsl.defaultUser = "nixos";

  programs.gnupg.agent.enable = true;

  i18n.defaultLocale = "bg_BG.UTF-8";

  environment.systemPackages = with pkgs; [
    vim git pass pinentry
    emacs mc file
    wget iosevka dejavu_fonts
    killall htop
    xorg.xmodmap xorg.xev
    binutils keyutils unzip
    lsof gnumake nmap
    gnupg libtool
    pgcli
    ispell aspell aspellDicts.bg aspellDicts.en
    sshpass mustache-go envsubst
    rclone
    jq maven gradle tree pandoc tetex awscli2 kafkacat
    nodePackages.bash-language-server
    nodePackages.yaml-language-server
    nodePackages.node2nix
    nodePackages.typescript-language-server
    kubectl docker
    cifs-utils
    dbeaver
    jmeter
    nodejs
    nixos-option
    lombok
    openconnect openssl
    jetbrains.idea-community
  ];

  fonts.packages = with pkgs; [
    iosevka
  ];

  environment.variables.EDITOR = "emacsclient";

#  services.xserver = {
#    enable = true;
#    autorun = true;
#    desktopManager.xfce.enable = true;
#
#    layout = "gb,bg";
#    xkbModel = "pc105";
#    xkbOptions = "grp:alt_shift_toggle,caps:escape";
#    libinput.touchpad.naturalScrolling = true;
#    libinput.enable = true;
#    resolutions = [ { x = 2560; y  = 1440; } { x = 3840; y  = 2160; } ];
#  };

  nixpkgs.config.allowUnfreePredicate = pkg: builtins.elem (lib.getName pkg) [
  ];

  virtualisation.docker.enable = true;
  programs.java.enable = true;

  nix.settings.experimental-features = [ "nix-command" "flakes "];

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It's perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.11"; # Did you read the comment?
}
